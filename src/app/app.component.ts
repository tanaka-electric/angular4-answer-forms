import { NgForm } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';

import {ToasterModule, ToasterService, ToasterConfig} from 'angular2-toaster';

// azure function用
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  private toasterService: ToasterService;
  
  public config1 : ToasterConfig = new ToasterConfig({
    positionClass: 'toast-top-right'
  });

  @ViewChild('tanakaForm')
  private tanakaForm: NgForm;

  constructor(private http: HttpClient, toasterService: ToasterService)
  {
    this.toasterService = toasterService;
  }

  // htmlテンプレートの(ngSubmit)="register(tanakaForm)" で呼ばれるアンケート登録ロジック
  register(tanakaForm: NgForm) {

    // ここでtanakaForm.value | json の answer（回答番号） をazure functionに通達する。
    const functionURI = 'https://telc-answer-func.azurewebsites.net/api/HttpTriggerJS1?code=wbHGua6bi6dWlAMpci7bWKHMMo2LOmHyynz9Eef6vFGmaow2tG6sQQ==';  
    
    this.http.post(functionURI, tanakaForm.value ).subscribe(data => {
      // クラウドへの通達結果をコンソールに表示
      console.log("azureへの通達結果-> " + data);

      // ユーザへのフィードバックとしてtoastを表示
      var toast = {
        type: 'info',
        title: '田中電機アンケート',
        body: 'ご回答ありがとうございます'
      };
      this.toasterService.pop(toast);
    });
  }
}
