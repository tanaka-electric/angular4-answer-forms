## これは何？

- Angular4ベースのアンケートアプリです。

## ビルド方法

### 前提条件

- Node.js v.7.x 以上
- Angular CLI v.1.6.x 以上 (※ `npm install -g @angular/cli` でインストール)

### 手順

いちど以下実施して Node パッケージを復元しておく。

```npm install```

あとは後述のとおり `ng` コマンドを用いることで、ローカル実行したり、発行用ファイルを生成したりする。

## ローカルで実行するには

ルートフォルダをコマンドライン表示して以下コマンドを実行する。

```ng serve --open``` 

すると Angular CI によって必要なファイルのビルドと、開発用サーバー (4200ポートでリッスンしている) の起動が行なわれ、かつ、`--open` オプションの指定によってデフォルトブラウザの起動まで行なわれる。

## デプロイ手順

### azure function へ HttpTriggerスクリプトを登録する
- ./azure/HttpTriggerJS1.js　を azure function へ登録する。
- functionのリリース共有（CORS）に、http://localhost:4200 と本番アプリのURLを追加する。
- functionのURLを、 ./src/app.component.ts　にペーストする。

``` const functionURI = 'https://<hogefuga>.azurewebsites.net/api/HttpTriggerJS1?code=<key>';``` 

### 本番用に最適化したjs/html群を生成

以下実施で ./dist に配置用ファイルができる。

```ng b -prod --base-href=/answer/```

ビルドオプション
- -prod: 本番用にコードを最適化
- --base: URLベース指定。※index.htmlのヘッダーの href base "/"　を  "/(azure blob名)/" に置換しておく。

### azure blob へ ./dist の中身を配置
- Azure Storage Explorerで./dist　の中身をすべてazure blobへコピーする。

### アンケ―トの集計

- Azure Storage Explorerで以下のテーブルを参照
- Strage Accounts -> (Azure Function名) -> Tables -> outTable 
- エクスポートする
- wish列を集計する
- ※　AzureWebJobsHostLogs(yyyymm)のLogOutput列には生のjsonも格納している。

### azure デプロイ結果

## 作ったソース
Microsoft Partner Network の 田中電機公開用フォルダ(jsakam...)で以下ソースを作った。
- Azure Storage - コンテナ名: answer
- Azure Function - 関数名: telc-answer-func - HttpTriggerJS1

## 本番URL
https://telc-answer-func.azurewebsites.net/answer/index.html

※functionsのweb ploxy機能により 
https://telcanswerfunc.blob.core.windows.net/answer/index.html
→
https://telc-answer-func.azurewebsites.net/answer/index.html　

に変更している。

### 参考

#### Angular4 HttpClientについて

- https://angular.io/guide/http

#### angular2-toasterについて

- https://www.npmjs.com/package/angular2-toaster