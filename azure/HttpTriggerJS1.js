module.exports = function (context, req) {
    var jsonText = JSON.stringify(req.body);
    context.res = {
        body: "[azure function] Received : " + jsonText
    };
    // 既存のログテーブルにjsonをそのまま追加します。
    context.log(jsonText);

    // 専用のテーブルにjsonを分割して追加します。
    context.bindings.outputTable = { 
        PartitionKey: 'MyPartitionKey',
        RowKey: generateRandomId(),
        wish: req.body.isBuy.wish
    }
    
    context.done();
};

// ランダムなRowIDを作る
function generateRandomId() {
    return Math.random().toString(36).substring(2, 15) +
        Math.random().toString(36).substring(2, 15);
}